﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    /**********************
     * Refer to photon documentation and scripting API if needed
     *
     * Documentation: https://doc.photonengine.com/en-us/pun/current/getting-started/pun-intro
     * Scripting API: https://doc-api.photonengine.com/en/pun/v2/
     *
     * If unity editor and build do not connect to each other, refer to these region settings
     * and then manually set the fixed region values of your photon server settings.
     * If there are different regions of editor and build, then the server might be different
     * and it would be hard to connect.
     * https://doc.photonengine.com/en-us/realtime/current/connection-and-authentication/regions
     */
    
    // instance
    public static NetworkManager instance;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            gameObject.SetActive(false);
        }
        else
        {
            // set the instance
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        PhotonNetwork.NickName = MasterManager.GameSettings.DisplayName;
        // Connects to photon Master Server
        PhotonNetwork.ConnectUsingSettings();
        
        // Other ways to connect are in Master Class: https://doc-api.photonengine.com/en/pun/v2/class_photon_1_1_pun_1_1_photon_network.html
    }

    public override void OnConnectedToMaster()
    {
        print("Connected to master Server on: " + PhotonNetwork.CloudRegion + " server!");
        print(PhotonNetwork.LocalPlayer.NickName);
      //  CreateRoom("testRoom");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        print("Disconnected from server: " + cause);
    }

    public void CreateRoom(string roomName)
    {
        PhotonNetwork.CreateRoom(roomName);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("Created room: " + PhotonNetwork.CurrentRoom.Name);
    }

    public void JoinRoom(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }

    public void ChangeScene(string sceneName)
    {
        PhotonNetwork.LoadLevel(sceneName);
    }
}
