﻿using UnityEngine;

namespace Managers
{
    public class SingletonScriptableObject<T> : ScriptableObject where T:ScriptableObject
    {
        private static T _instance = null;

        protected static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    T[] results = Resources.FindObjectsOfTypeAll<T>();
                    if (results.Length == 0)
                    {
                        Debug.LogError("Singleton length is 0");
                        return null;
                    }

                    if (results.Length > 1)
                    {
                        Debug.LogError("Singleton length is greater than 1");
                        return null;
                    }

                    _instance = results[0];
                }
                return _instance;
            }
        }
    
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
