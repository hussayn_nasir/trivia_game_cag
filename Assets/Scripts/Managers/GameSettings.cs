﻿using UnityEngine;

namespace Managers
{
    [CreateAssetMenu(menuName = "Manager/GameSettings")]
    public class GameSettings : ScriptableObject
    {
        // [SerializeField] private string _gameVersion = "0.0.0";
        //
        // public string GameVersion => _gameVersion;

        [SerializeField] private string _displayName = "HH1";

        public string DisplayName
        {
            get
            {
                int value = Random.Range(0, 9999);
                return _displayName + value.ToString();
            }
        }
    
    }
}
