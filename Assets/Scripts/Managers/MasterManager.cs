﻿using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;

[CreateAssetMenu(menuName = "Singletons/MasterManager")]
public class MasterManager : SingletonScriptableObject<MasterManager>
{
    [SerializeField] 
    private GameSettings _gameSettings;

    public static GameSettings GameSettings
    {
        get => Instance._gameSettings;
        set => Instance._gameSettings = value;
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    private static void FirstInitialize()
    {
        DontDestroyOnLoad(GameSettings);
    }
    
}

