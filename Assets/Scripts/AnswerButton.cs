﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour {

    public Text answerText;

    private AnswerData m_AnswerData;
    private GameController m_GameController;

    // Use this for initialization
    void Start () 
    {
        m_GameController = FindObjectOfType<GameController> ();
    }

    public void Setup(AnswerData data)
    {
        m_AnswerData = data;
        answerText.text = m_AnswerData.answerText;
    }


    public void HandleClick()
    {
        m_GameController.AnswerButtonClicked (m_AnswerData.isCorrect);
    }
}