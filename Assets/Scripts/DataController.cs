﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class DataController : MonoBehaviour 
{
    private RoundData[] allRoundData;

    private PlayerProgress m_PlayerProgress;
    private String gameDataFileName = "data.json";
    
    // Fetch json from server

    public string jsonURL;

    // Use this for initialization
    void Start ()  
    {
        DontDestroyOnLoad (gameObject);
        LoadGameData();
        // StartCoroutine(LoadGameDataFromWeb());
        LoadPlayerProgress();

        SceneManager.LoadScene ("MenuScreen");
    }

    public RoundData GetCurrentRoundData()
    {
        return allRoundData [0];
    }

    public void SubmitNewPlayerScore(int newScore)
    {
        if (newScore > m_PlayerProgress.highestScore)
        {
            m_PlayerProgress.highestScore = newScore;
            SavePlayerProgress();
        }
    }

    public int GetHighestPlayerScore()
    {
        return m_PlayerProgress.highestScore;
    }

    private void LoadPlayerProgress()
    {
        m_PlayerProgress = new PlayerProgress();

        if (PlayerPrefs.HasKey("highestScore"))
        {
            m_PlayerProgress.highestScore = PlayerPrefs.GetInt("highestScore");
        }
    }

    private void SavePlayerProgress()
    {
        PlayerPrefs.SetInt("highestScore", m_PlayerProgress.highestScore);
    }

    private void LoadGameData()
    {
        string filePath;
        if (Application.platform == RuntimePlatform.Android)
        {
            filePath = Path.Combine("jar:file://", Application.dataPath, "/assets", gameDataFileName);
            if (!File.Exists(filePath))
            {
                Debug.Log("File not found on android at " + filePath);
            }
        }
        else
        {
            filePath = Path.Combine(Application.streamingAssetsPath, gameDataFileName);
        }

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);

            allRoundData = loadedData.allRoundData;

        }
        else
        {
            Debug.LogError("The game data cannot be loaded. Check cs.58 at DataController.cs");
        }
    }


    private IEnumerator LoadGameDataFromWeb()
    {
        Debug.Log("Fetching Data, Please Wait...");
        UnityWebRequest _www = UnityWebRequest.Get(jsonURL);
        string filePath = Path.Combine(jsonURL, gameDataFileName);
       
        //wait for link to load
        yield return _www.SendWebRequest();
        
        //check if file exists
        if (_www.error == null)
        {
            Debug.Log("File Found at url: " + filePath);
            string _url = File.ReadAllText(filePath);
            GameData loadedDataFromWeb = JsonUtility.FromJson<GameData>(_url);

            allRoundData = loadedDataFromWeb.allRoundData;
        }
        else
        {
            Debug.LogError("File could not be found at url: " + filePath);
        }
        
    }

    private void ProcessJsonData()
    {
        
    }
    
    
    
    
    
}