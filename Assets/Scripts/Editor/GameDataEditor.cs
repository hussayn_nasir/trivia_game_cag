﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class GameDataEditor : EditorWindow
{
    public GameData gameData;

    public QuestionData questionData;
    
    private string gameDataProjectFilePath = "data.json";

    [MenuItem("Window/Game Data Editor")]
    static void Init()
    {
        GameDataEditor window = (GameDataEditor) EditorWindow.GetWindow(typeof(GameDataEditor));
        window.Show();
    }

    private void OnGUI()
    {
        if (gameData != null)
        {
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("gameData");
            // SerializedProperty serializedProperty = serializedObject.FindProperty("questionData");
               

            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Save Data"))
            {
                SaveGameData();
            }
        }

        if (GUILayout.Button("Load Data"))
        {
            LoadGameData();
        }
    }

    private void LoadGameData()
    {
        string filePath;
        if (Application.platform == RuntimePlatform.Android)
        {
            filePath = Path.Combine("jar:file://", Application.streamingAssetsPath, gameDataProjectFilePath);
        }
        else
        {
            filePath = Path.Combine(Application.streamingAssetsPath, gameDataProjectFilePath);
        }
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            gameData = JsonUtility.FromJson<GameData>(dataAsJson);
        }
        else
        {
            gameData=new GameData();
        }
    }

    private void SaveGameData()
    {
        string dataAsJson = JsonUtility.ToJson(gameData);
        string filePath = Application.streamingAssetsPath + gameDataProjectFilePath;
        File.WriteAllText(filePath,dataAsJson);
    }
    
   
}
