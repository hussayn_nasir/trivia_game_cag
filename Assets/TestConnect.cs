﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class TestConnect : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.NickName = MasterManager.GameSettings.DisplayName;
        print(PhotonNetwork.NickName + " is connecting to server...");
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        print("Player: " + PhotonNetwork.LocalPlayer.NickName + " has connected to " + PhotonNetwork.CloudRegion + " server.");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        print("Disconnected due to reason: " + cause);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
